var bs_d = {};
var bs_l = [];


function honorMyTime(name, link)
{
	if (bs_l.indexOf(link) >=0)
		return;

	bs_l.push(link);
	bs_d[link] = name;

	browser.storage.local.set({bs_l, bs_d});

	console.log('#I : yt bs block : blocking : ' + name + ' : ' + link)
}

function giveMeTheChoice(parent)
{
	var e,el;

	if (!parent)
		return;

	e = document.createElement('A');

	e.style.width = '120px';
	e.style.margin = '5px';
	e.style.fontSize = '13px';
	e.style.color = '#A44';
	e.innerText = 'Block BS channel';
	e.className = 'i-will-save-thou';

	e.onclick=function(ev){
		var e;

		e = (ev.target || ev.originalTarget).parentElement.querySelector('a.yt-simple-endpoint.yt-formatted-string');
		honorMyTime(e.innerText, e.href);

		ev.preventDefault();
		ev.stopPropagation();
	}

	parent.appendChild(e);
}

function isInNeedOfSaving(e)
{
	return e.querySelector('.i-will-save-thou') === null;
}

function isBS(e)
{
	var name_e = e.querySelector('#channel-name a');
	return name_e && (bs_l.indexOf(name_e.href) >= 0);
}

function surgeThisWellOrMakeItPerish(parent_q, choice_q)
{
	var i, el;

	el = document.body.querySelectorAll(parent_q);

	for (i = 0; i < el.length; i++) {
		if (isBS(el[i]))
			el[i].remove();
		else
		if (isInNeedOfSaving(el[i]))
			giveMeTheChoice(el[i].querySelector(choice_q));
	}
}

function testMyPatience()
{

	surgeThisWellOrMakeItPerish('ytd-video-renderer', '#channel-info');// Search results
	surgeThisWellOrMakeItPerish('ytd-rich-item-renderer', 'ytd-channel-name');// Main page

	setTimeout(testMyPatience, 1000);
}


browser.storage.onChanged.addListener(function (itm, area) {
	if (area != 'local')
		return;

	bs_l = itm.bs_l.newValue;
	bs_d = itm.bs_d.newValue;
});

browser.storage.local.get().then(
	function (itm) {
		bs_l = itm.bs_l || [];
		bs_d = itm.bs_d || {};

		testMyPatience();
	},
	function () {
	}
);
