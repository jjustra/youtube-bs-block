// requires tea.js

var bd_l,bs_d;

function save()
{
	browser.storage.local.set({bs_l,bs_d}).then(
		()=>{
			I('youtube-bs-block : saved',bs_l,bs_d)
		},
		()=>{ E('youtube-bs-block : save : error') },
	)
}

function render()
{
	var i,e;

	// Clear
	e = TG();
	while (e.children.length > 1)
		e.children[1].remove();

	// Create new list of butts
	for (i = 0; i < bs_l.length; i++) {

		let k = bs_l[i];

		TX('B', bs_d[k] + ' - ' + k, (d,p)=>{
			bs_l.splice(bs_l.indexOf(k), 1);
			delete bs_d[k];
			save();
		});

	}
}

function load()
{
	browser.storage.local.get().then(
		(o)=>{
			// Fetch
			bs_l = o.bs_l || [];
			bs_d = o.bs_d || {};

			render();

			I('youtube-bs-block : loaded',bs_l,bs_d);
		},
		()=>{ E('youtube-bs-block : load : error') },
	)
}


TC('B_s', 'text:align:left;')
TX('Gwidth:98%;position:relativeLclick to unblock :');

browser.storage.onChanged.addListener(function (itm, area) {
	if (area != 'local')
		return;

	bs_l = itm.bs_l.newValue;
	bs_d = itm.bs_d.newValue;

	render();
});

load();
